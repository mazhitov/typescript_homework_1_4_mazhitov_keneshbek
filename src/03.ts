class KeyValueDictionaryPair<TKey, TValue, TDescription> {
    private _key: TKey;
    private _value: TValue;
    private _description: TDescription;

    set key(value: TKey) {
        if (!value) {
            throw new Error('Значение не должно быть пустым');
        }
        this._key = value;
    }

    get key(): TKey {
        return this._key;
    }

    set value(value: TValue) {
        if (!value) {
            throw new Error('Значение не должно быть пустым');
        }
        this._value = value;
    }

    get value(): TValue {
        return this._value;
    }

    set description(value: TDescription) {
        if (!value) {
            throw new Error('Значение не должно быть пустым');
        }
        this._description = value;
    }

    get description(): TDescription {
        return this._description;
    }
}

class Dictionary<TKey, TValue, TDescription> {
    private data: KeyValueDictionaryPair<TKey, TValue, TDescription>[] = [];

    add(key: TKey, value: TValue, description: TDescription) {
        let entry = new KeyValueDictionaryPair<TKey, TValue, TDescription>();
        entry.key = key;
        entry.value = value;
        entry.description = description;
        this.data.push(entry);
    }

    getValue(key: TKey): [TValue, TDescription] | null {
        for (let i = 0; i < this.data.length; i++) {
            if (this.data[i].key === key) {
                return [this.data[i].value, this.data[i].description]
            }
        }
        return null;
    }
}

let dictionary = new Dictionary<string, string, string>();
dictionary.add('hello', 'привет', 'приветствие');
dictionary.add('book', 'книга', 'вещь');
dictionary.add('apple', 'яблоко', 'фрукт');

console.log(dictionary.getValue('book'));