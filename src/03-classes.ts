import { Honda, Mercedes } from './02';

let mercedes = new Mercedes("Mercedes-Benz GLA");
let honda = new Honda("Honda City")

mercedes.run();  // A Mercedes started A Mercedes-Benz GLA is moving at 150 mph!
honda.run(); // A Honda started A Honda City is moving at 100 mph!
