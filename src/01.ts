const getCostIceCream = (glass: number, filling = 0): number => {
    let total = 0;
    switch (glass) {
        case 1:
            total += 10;
            break;
        case 2:
            total += 25;
            break;
    }
    if (filling) {
        switch (filling) {
            case 1:
                total += 5;
                break;
            case 2:
                total += 6;
                break;
            case 3:
                total += 10;
                break;
        }
    }
    return total;
};
const run = () => {
    const glassType = +prompt(`
    Выберите тип стакана:
    1. Маленький
    2. Большой
`);
    if (isNaN(glassType)) {
        return;
    }
    let fillingType = 0;
    if(glassType === 2) {
        fillingType = +prompt(`
    Выберите тип стакана:
    1. Шоколад
    2. Карамель
    3. Ягоды
`);
        if (isNaN(fillingType)) {
            return;
        }
    }
    console.log(getCostIceCream(glassType, fillingType));
    alert('Вы должны заплатитть ' + getCostIceCream(glassType, fillingType) + 'денег');
};

document.addEventListener('DOMContentLoaded', () => {
    run();
});